package com.example.toik_netflix.service;

import com.example.toik_netflix.dto.CreateMovieDto;
import com.example.toik_netflix.dto.MovieListDto;


public interface MovieRepository {
    MovieListDto getMovies();
    void addMovie(CreateMovieDto createMovieDto);
}
