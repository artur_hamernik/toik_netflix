package com.example.toik_netflix.service.impl;

import com.example.toik_netflix.dto.CreateMovieDto;
import com.example.toik_netflix.dto.MovieDto;
import com.example.toik_netflix.dto.MovieListDto;
import com.example.toik_netflix.service.MovieRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    private MovieListDto movies;
    private int counter = 1;

    public MovieRepositoryImpl() {
        List<MovieDto> movieDtos = new ArrayList<>();
        movieDtos.add(new MovieDto(counter++, "Piraci z krzemowej doliny", 1994, "image.jpg"));
        movieDtos.add(new MovieDto(counter++, "Ja, robot", 2004, "image.jpg"));
        movieDtos.add(new MovieDto(counter++, "Kod nieśmiertelności", 2011, "image.jpg"));
        movieDtos.add(new MovieDto(counter++, "Ex Machina", 2015, "image.jpg"));
        movies = new MovieListDto(movieDtos);
    }

    @Override
    public void addMovie(CreateMovieDto movie) {
        this.movies.getMovies().add(new MovieDto(counter++, movie.getTitle(), movie.getYear(),movie.getImage()));
    }

    @Override
    public MovieListDto getMovies() {
        return movies;
    }
}
