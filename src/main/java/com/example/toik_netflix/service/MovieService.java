package com.example.toik_netflix.service;

import com.example.toik_netflix.dto.MovieListDto;

public interface MovieService {
    MovieListDto getMovies();
}
